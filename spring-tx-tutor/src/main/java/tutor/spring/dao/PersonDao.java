package tutor.spring.dao;

import java.util.List;

import tutor.spring.model.Person;

public interface PersonDao {

	public void save(Person person);
	
	public List<Person> list();
	
}
