package tutor.spring;

import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

public class YetAnotherBookingService {

	private BookingService bookingService;
    
    @Transactional
    public void bookFamilies(Map<String, List<String>> families) {
        for (String family : families.keySet()) {
        	bookingService.bookFamily(families.get(family));
        }
    }

	public void setBookingService(BookingService bookingService) {
		this.bookingService = bookingService;
	}
    
}
