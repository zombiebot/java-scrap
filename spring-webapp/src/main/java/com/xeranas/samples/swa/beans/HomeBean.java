package com.xeranas.samples.swa.beans;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.xeranas.samples.swa.api.DataManager;
import com.xeranas.samples.swa.dto.Information;

public class HomeBean implements Serializable {

	private static final long serialVersionUID = -7889256642067096581L;
	
	private String wellcomeText = "Wellcome!";
	
	private DataManager dataManager;
	
	private Information information = new Information();
	
	public void saveInformation() {
		dataManager.saveInfo(information);
		clearInformation();
	}
	
	public List<Information> getData() {
		return dataManager.retrieveAll();
	}
	
	private void clearInformation() {
		information = new Information();
	}
	
	public String getWellcomeText() {
		return wellcomeText;
	}

	public void setWellcomeText(String wellcomeText) {
		this.wellcomeText = wellcomeText;
	}

	public Information getInformation() {
		return information;
	}

	public void setInformation(Information information) {
		this.information = information;
	}

	@Required
	public void setDataManager(DataManager dataManager) {
		this.dataManager = dataManager;
	}
	
}
