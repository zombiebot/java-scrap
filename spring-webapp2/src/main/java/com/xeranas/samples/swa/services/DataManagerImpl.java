package com.xeranas.samples.swa.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import com.xeranas.samples.swa.api.DataManager;
import com.xeranas.samples.swa.dto.Information;

public class DataManagerImpl implements DataManager {
	
	private Map<Long, Information> data = new HashMap<Long, Information>();
	private AtomicLong index = new AtomicLong(0);
	
	public void saveInfo(Information info) {
		index.incrementAndGet();
		info.setId(index.get());
		info.setDate(new Date());
		data.put(index.get(), info);
	}

	public Information retriveInfo(Long id) {
		return data.get(id);
	}

	public List<Information> retrieveAll() {
		return new ArrayList<Information>(data.values());
	}

}
